import socket
import sys

def main():
    soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    host = "127.0.0.1"
    port = 8888

    try:
        soc.connect((host, port))
    except:
        print("Connection error")
        sys.exit()

    received_msg = soc.recv(5120).decode("utf8")
    print(received_msg)

    message = input(" -> ")

    while message != 'quit':
        soc.sendall(message.encode("utf8"))
        received_msg = soc.recv(5120).decode("utf8")
        print(received_msg)

        message = input(" -> ")

    soc.send(b'quit')

if __name__ == "__main__":
    main()
