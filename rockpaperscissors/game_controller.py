from rpc_game import RpcEngine, InputParser, RpcResult
from enum import Enum

class GameControllerState(Enum):
    WAITING_FOR_PLAYERS = 1
    WAITING_FOR_PLAYER2 = 2
    GAME_NOT_STARTED = 3
    GAME_READY = 4
    GAME_ONGOING = 5
    GAME_COMPLETE = 6
    GAME_ERROR = 99

class GameController:

    DEFAULT_ROUNDS = 3

    def __init__(self):
        self.state = GameControllerState.WAITING_FOR_PLAYERS
        self.result = RpcResult.NO_RESULT
        self.rounds = self.DEFAULT_ROUNDS

    def add_player(self, player):
        if (self.state == GameControllerState.WAITING_FOR_PLAYERS):
            self.player1 = player
            self.state = GameControllerState.WAITING_FOR_PLAYER2
        elif (self.state == GameControllerState.WAITING_FOR_PLAYER2):
            self.player2 = player
            self.state = GameControllerState.GAME_NOT_STARTED
        else:
            self.state = GameControllerState.GAME_ERROR

    def check_players(self):
        player1Check = InputParser.is_name_valid(self.player1)
        player2Check = InputParser.is_name_valid(self.player2)
        if ((not player1Check) and (not player2Check)):
            self.result = RpcResult.NO_RESULT
            self.state = GameControllerState.GAME_COMPLETE
        elif ((not player1Check) and (player2Check)):
            self.result = RpcResult.PLAYER_2_WIN
            self.state = GameControllerState.GAME_COMPLETE
        elif ((player1Check) and (not player2Check)):
            self.result = RpcResult.PLAYER_1_WIN
            self.state = GameControllerState.GAME_COMPLETE
        else:
            self.rpcEngine = RpcEngine(self.player1, self.player2)
            self.rpcEngine.set_rounds(self.rounds)
            self.state = GameControllerState.GAME_READY

    def play_round(self, value1, value2):
        if (self.state == GameControllerState.GAME_READY):
            self.state = GameControllerState.GAME_ONGOING
        
        if (self.state == GameControllerState.GAME_ONGOING):
            if (self.rpcEngine.roundsRemaining > 0):
                self.rpcEngine.play_round(value1, value2)
                if (self.rpcEngine.roundsRemaining == 0):
                    self.result = self.rpcEngine.get_result()
                    self.state = GameControllerState.GAME_COMPLETE
                else:
                    # Game still ongoing
                    pass
            else:
                # Game complete, shouldn't be here
                GameControllerState.GAME_ERROR
        else:
            # Should only be called if GAME_ONGOING
            GameControllerState.GAME_ERROR
