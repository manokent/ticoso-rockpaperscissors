from enum import Enum

class RpcValue(Enum):
    ROCK = 1
    PAPER = 2
    SCISSORS = 3
    UNKNOWN = 4

class RpcResult(Enum):
    PLAYER_1_WIN = 1
    PLAYER_2_WIN = 2
    NO_RESULT = 3
    DRAW = 4

class InputParser:

    @staticmethod
    def get_rpc_value(input):
        result = RpcValue.UNKNOWN

        if (isinstance(input, str) == True):
            if (input.lower() == 'rock'):
                result = RpcValue.ROCK
            elif (input.lower() == 'paper'):
                result = RpcValue.PAPER
            elif (input.lower() == 'scissors'):
                result = RpcValue.SCISSORS

        return result

    @staticmethod
    def is_name_valid(input):
        return isinstance(input, str)

class RpcEngine:

    PLAYER_1_DEFAULT_NAME = "Player1-Unknown"
    PLAYER_2_DEFAULT_NAME = "Player2-Unknown"
    
    def __init__(self, player1, player2):
        if (InputParser.is_name_valid(player1)):
            self.player1 = player1
        else:
            self.player1 = self.PLAYER_1_DEFAULT_NAME

        if (InputParser.is_name_valid(player2)):
            self.player2 = player2
        else:
            self.player2 = self.PLAYER_2_DEFAULT_NAME

        self.rounds = 1
        self.roundsRemaining = self.rounds
        self.player1Score = 0
        self.player2Score = 0

    def reset(self):
        self.roundsRemaining = self.rounds
        self.player1Score = 0
        self.player2Score = 0

    def set_rounds(self, rounds):
        if (isinstance(rounds, int)):
            self.rounds = rounds
        self.reset()

    def play_round(self, player1Input, player2Input):
        parsed1Input = InputParser.get_rpc_value(player1Input)
        parsed2Input = InputParser.get_rpc_value(player2Input)

        if ((parsed1Input == RpcValue.UNKNOWN) and (parsed2Input != RpcValue.UNKNOWN)):
            self.player2Score += 1
        elif ((parsed1Input != RpcValue.UNKNOWN) and (parsed2Input == RpcValue.UNKNOWN)):
            self.player1Score += 1
        else:
            if (parsed1Input == RpcValue.ROCK):
                if (parsed2Input == RpcValue.PAPER):
                    self.player2Score += 1
                elif (parsed2Input == RpcValue.SCISSORS):
                    self.player1Score += 1
            elif (parsed1Input == RpcValue.PAPER):
                if (parsed2Input == RpcValue.SCISSORS):
                    self.player2Score += 1
                elif (parsed2Input == RpcValue.ROCK):
                    self.player1Score += 1
            elif (parsed1Input == RpcValue.SCISSORS):
                if (parsed2Input == RpcValue.ROCK):
                    self.player2Score += 1
                elif (parsed2Input == RpcValue.PAPER):
                    self.player1Score += 1

        self.roundsRemaining -= 1

    def get_result(self):
        result = RpcResult.NO_RESULT

        if (self.roundsRemaining > 0):
            result = RpcResult.NO_RESULT
        elif (self.roundsRemaining == 0):
            if (self.player1Score > self.player2Score):
                result = RpcResult.PLAYER_1_WIN
            elif (self.player1Score < self.player2Score):
                result = RpcResult.PLAYER_2_WIN
            else:
                result = RpcResult.DRAW

        return result