import socket
import sys
import traceback
import threading
from time import sleep
from threading import Thread
from game_controller import GameController, GameControllerState
from rpc_game import RpcResult, InputParser, RpcValue
import random_choices

def main():
    game_server = GameServer()
    game_server.start_server()

class GameServer:

    NUM_ROUNDS = 3
    BONUS_ANSWER = "marvel"
    
    def __init__(self):
        pass

    def start_server(self):
        host = "192.168.0.49"
        port = 8888

        soc = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        soc.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)   # SO_REUSEADDR flag tells the kernel to reuse a local socket in TIME_WAIT state, without waiting for its natural timeout to expire
        print("Socket created")

        try:
            soc.bind((host, port))
        except:
            print("Bind failed. Error : " + str(sys.exc_info()))
            sys.exit()

        soc.listen(5)       # queue up to 5 requests
        print("Socket now listening")

        while True:
            connection, address = soc.accept()
            ip, port = str(address[0]), str(address[1])
            print("New challenger connected with " + ip + ":" + port)
            player_thread = Thread(target=self.client_thread, args=(connection, ip, port))
            player_thread.start()

            # infinite loop- do not reset for every requests
            while player_thread.isAlive():
                pass

            print("Thanks for playing")

        soc.close()

    def client_thread(self, connection, ip, port, max_buffer_size = 5120):
        is_active = True
        bonus_round = False
        game_controller  = GameController()
        game_controller.rounds = self.NUM_ROUNDS
        connection.sendall("Welcome to Rock Paper Scissors, what is your name (name=<name>)\n".encode("utf8"))

        while is_active:
            client_input = self.receive_input(connection, max_buffer_size)
            print("Received - {}".format(client_input))

            if "QUIT" in client_input:
                print("Client is requesting to quit")
                self.close_connection(connection, ip, port)
                is_active = False
            elif client_input == "":
                print("Nothing returned from the client, closing the connection")
                self.close_connection(connection, ip, port)
                is_active = False
            elif game_controller.state == GameControllerState.WAITING_FOR_PLAYERS:
                if "NAME=" in client_input:
                    player_name = self.get_value(client_input).title()
                    game_controller.add_player(player_name)
                    computer_name = random_choices.get_random_marvel_name()
                    game_controller.add_player(computer_name)

                    game_controller.check_players()
                    if game_controller.state == GameControllerState.GAME_COMPLETE:
                        self.announce_result(connection, game_controller, player_name, computer_name)
                        self.close_connection(connection, ip, port)
                        is_active = False
                    elif game_controller.state == GameControllerState.GAME_READY:
                        self.send_line(connection,
                            "Hello {}, you'll be playing against {} in a game of {} rounds".format(player_name, computer_name, self.NUM_ROUNDS))
                        self.send_line(connection, "Rock, Paper or Scissors? (choice=<value>)")
                    else:
                        self.send_line(connection, "Something unexpected has happened, I better go")
                        self.close_connection(connection, ip, port)
                        is_active = False
                else:
                    msg = "I don't know what '{}' is, what is your name? (name=<name>)".format(client_input)
                    connection.sendall("{}\n".format(msg).encode("utf8"))
            elif game_controller.state == GameControllerState.GAME_READY or game_controller.state == GameControllerState.GAME_ONGOING:
                if "CHOICE=" in client_input:
                    player_choice = self.get_value(client_input).lower()
                    if (InputParser.get_rpc_value(player_choice) == RpcValue.UNKNOWN):
                        self.send_line(connection, "I don't recognise \"{}\", you're going to lose this round".format(player_choice))

                    computer_choice = random_choices.get_random_rps_choice()
                    self.send_line(connection, "{} has chosen {}\n{} has chosen {}".format(player_name, player_choice, computer_name, computer_choice))
                    game_controller.play_round(player_choice, computer_choice)
                    self.send_line(connection, "{} - {} : {} - {}".format(
                        player_name, game_controller.rpcEngine.player1Score, computer_name, game_controller.rpcEngine.player2Score))

                    if game_controller.state == GameControllerState.GAME_COMPLETE:
                        self.announce_result(connection, game_controller, player_name, computer_name)
                        if (game_controller.result == RpcResult.PLAYER_1_WIN):
                            bonus_round = True
                            msg = "For bonus kudos, can you guess the one word answer which links the computer game names? One chance (bonus=<answer>)"
                            self.send_line(connection, msg)
                        else:
                            self.close_connection(connection, ip, port)
                            is_active = False
                    else:
                        self.send_line(connection, "Rock, Paper or Scissors? (choice=<value>)")
                else:
                    msg = "I don't know what '{}' is, Rock, Paper or Scissors? (choice=<value>)".format(client_input)
                    connection.sendall("{}\n".format(msg).encode("utf8"))
                    print(msg)
            elif game_controller.state == GameControllerState.GAME_COMPLETE and bonus_round:
                if "BONUS=" in client_input:
                    bonus_answer = self.get_value(client_input).lower()
                    msg = "Sorry, {} is wrong. Bye.".format(bonus_answer)
                    if (bonus_answer == self.BONUS_ANSWER):
                        msg = "Well done {} is correct! Bye".format(bonus_answer)
                    self.send_line(connection, msg)
                else:
                    self.send_line(connection, "Sorry, you've failed to answer the bonus question. Bye")
                
                self.close_connection(connection, ip, port)
                is_active = False
            else:
                self.send_line(connection, "Unknown game state")
                self.close_connection(connection, ip, port)
                is_active = False

        print("Exiting thread")

    def get_value(self, name_value):
        name_value_split = name_value.split("=", 1)
        return name_value_split[1].strip()

    def send_line(self, connection, msg_line):
        print("Sending - {}".format(msg_line))
        connection.sendall("{}\n".format(msg_line).encode("utf8"))

    def close_connection(self, connection, ip, port):
        connection.sendall("This connection is closing\n".encode("utf8"))
        connection.close()
        print("Connection " + ip + ":" + port + " closed")

    def announce_result(self, connection, game_controller, player_name, computer_name):
        if (game_controller.result == RpcResult.PLAYER_1_WIN):
            msg = "{} has won, congratulations, this must be a real highlight for you\n".format(player_name)
        elif (game_controller.result == RpcResult.PLAYER_2_WIN):
            msg = "{} has won, you've been beaten by a random number generator, which is awkward\n".format(computer_name)
        elif (game_controller.result == RpcResult.DRAW):
            msg = "It was a draw, riverting stuff\n"

        connection.sendall(msg.encode("utf8"))

    def receive_input(self, connection, max_buffer_size):
        result = ""
        client_input = connection.recv(max_buffer_size)
        client_input_size = sys.getsizeof(client_input)

        if (client_input_size > 0):
            if client_input_size > max_buffer_size:
                print("Client input size is greater than expected {}".format(client_input_size))

            decoded_input = client_input.decode("utf8").rstrip()  # decode and strip end of line
            result = decoded_input.upper()
        else:
            pass

        return result

if __name__ == "__main__":
    main()
