import unittest
from rockpaperscissors.rpc_game import InputParser, RpcValue, RpcEngine, RpcResult

class TestInputParser(unittest.TestCase):

    def test_get_rpc_value_rock(self):
        rpcValue = InputParser.get_rpc_value('rock')
        self.assertEqual(RpcValue.ROCK, rpcValue)

    def test_get_rpc_value_paper(self):
        rpcValue = InputParser.get_rpc_value('paper')
        self.assertEqual(RpcValue.PAPER, rpcValue)

    def test_get_rpc_value_scissors(self):
        rpcValue = InputParser.get_rpc_value('scissors')
        self.assertEqual(RpcValue.SCISSORS, rpcValue)

    def test_get_rpc_value_uppercase(self):
        rpcValue = InputParser.get_rpc_value('SCISSORS')
        self.assertEqual(RpcValue.SCISSORS, rpcValue)

    def test_get_rpc_value_mixed_case(self):
        rpcValue = InputParser.get_rpc_value('RoCk')
        self.assertEqual(RpcValue.ROCK, rpcValue)

    def test_get_rpc_value_empty_value(self):
        rpcValue = InputParser.get_rpc_value('')
        self.assertEqual(RpcValue.UNKNOWN, rpcValue)

    def test_get_rpc_value_random_value(self):
        rpcValue = InputParser.get_rpc_value('sd.fm.dsm111')
        self.assertEqual(RpcValue.UNKNOWN, rpcValue)

    def test_get_rpc_value_int_value(self):
        rpcValue = InputParser.get_rpc_value(1234)
        self.assertEqual(RpcValue.UNKNOWN, rpcValue)

    def test_get_rpc_value_boolean_value(self):
        rpcValue = InputParser.get_rpc_value(False)
        self.assertEqual(RpcValue.UNKNOWN, rpcValue)

class TestRpcEngine(unittest.TestCase):

    def test_rounds_init(self):
        rpcEngine = RpcEngine("a", "b")
        self.assertEqual(1, rpcEngine.rounds)

    def test_set_rounds_valid(self):
        rpcEngine = RpcEngine("a", "b")
        expected = 3
        rpcEngine.set_rounds(expected)
        self.assertEqual(expected, rpcEngine.rounds)

    def test_set_rounds_invalid_string(self):
        rpcEngine = RpcEngine("a", "b")
        expected = 1
        rpcEngine.set_rounds("12345")
        self.assertEqual(expected, rpcEngine.rounds)

    def test_get_result_not_started(self):
        rpcEngine = RpcEngine("a", "b")
        self.assertEqual(RpcResult.NO_RESULT, rpcEngine.get_result())

    def test_get_result_multiple_rounds(self):
        rpcEngine = RpcEngine("a", "b")
        rpcEngine.set_rounds(3)
        rpcEngine.play_round("rock", "paper")
        rpcEngine.play_round("scissors", "paper")
        rpcEngine.play_round("paper", "rock")
        self.assertEqual(0, rpcEngine.roundsRemaining)
        self.assertEqual(RpcResult.PLAYER_1_WIN, rpcEngine.get_result())

    def test_play_round_p1_unknown_input(self):
        rpcEngine = RpcEngine("a", "b")
        rpcEngine.play_round("sdfhdskf", "rock")
        self.assertEqual(RpcResult.PLAYER_2_WIN, rpcEngine.get_result())

    def test_play_round_p2_unknown_input(self):
        rpcEngine = RpcEngine("a", "b")
        rpcEngine.play_round("scissors", "rrock")
        self.assertEqual(RpcResult.PLAYER_1_WIN, rpcEngine.get_result())

    def test_play_round_valid_p1_rock_p2_paper(self):
        rpcEngine = RpcEngine("a", "b")
        rpcEngine.play_round("rock", "paper")
        self.assertEqual(RpcResult.PLAYER_2_WIN, rpcEngine.get_result())

    def test_play_round_valid_p1_rock_p2_scissors(self):
        rpcEngine = RpcEngine("a", "b")
        rpcEngine.play_round("rock", "scissors")
        self.assertEqual(RpcResult.PLAYER_1_WIN, rpcEngine.get_result())

    def test_play_round_valid_p1_rock_p2_rock(self):
        rpcEngine = RpcEngine("a", "b")
        rpcEngine.play_round("rock", "rock")
        self.assertEqual(RpcResult.DRAW, rpcEngine.get_result())

    def test_play_round_valid_p1_paper_p2_rock(self):
        rpcEngine = RpcEngine("a", "b")
        rpcEngine.play_round("paper", "rock")
        self.assertEqual(RpcResult.PLAYER_1_WIN, rpcEngine.get_result())

    def test_play_round_valid_p1_scissors_p2_rock(self):
        rpcEngine = RpcEngine("a", "b")
        rpcEngine.play_round("scissors", "rock")
        self.assertEqual(RpcResult.PLAYER_2_WIN, rpcEngine.get_result())

    def test_play_round_valid_p1_paper_p2_paper(self):
        rpcEngine = RpcEngine("a", "b")
        rpcEngine.play_round("paper", "paper")
        self.assertEqual(RpcResult.DRAW, rpcEngine.get_result())

    def test_play_round_valid_p1_scissors_p2_scissors(self):
        rpcEngine = RpcEngine("a", "b")
        rpcEngine.play_round("scissors", "scissors")
        self.assertEqual(RpcResult.DRAW, rpcEngine.get_result())

    def test_player_1_name_valid_name(self):
        expected = "Bob"
        rpcEngine = RpcEngine(expected, "Eve")
        self.assertEqual(expected, rpcEngine.player1)

    def test_player_2_name_valid_name(self):
        expected = "Eve"
        rpcEngine = RpcEngine("Bob", expected)
        self.assertEqual(expected, rpcEngine.player2)

    def test_player_1_name_invalid_name(self):
        rpcEngine = RpcEngine(12345, "Eve")
        self.assertEqual(RpcEngine.PLAYER_1_DEFAULT_NAME, rpcEngine.player1)

    def test_player_2_name_invalid_name(self):
        rpcEngine = RpcEngine("Bob", 939873298)
        self.assertEqual(RpcEngine.PLAYER_2_DEFAULT_NAME, rpcEngine.player2)

if __name__ == '__main__':
    unittest.main()