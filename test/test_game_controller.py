import unittest
from rockpaperscissors.game_controller import GameController, GameControllerState
from rockpaperscissors.rpc_game import RpcResult

class TestGameController(unittest.TestCase):

    def test_result_init(self):
        gc = GameController()
        self.assertEqual(RpcResult.NO_RESULT, gc.result)

    def test_state_init(self):
        gc = GameController()
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYERS, gc.state)

    def test_add_player_add_first_player(self):
        gc = GameController()
        gc.add_player("Player 1")
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYER2, gc.state)

    def test_add_player_add_second_player(self):
        gc = GameController()
        gc.add_player("Player 1")
        gc.add_player("Player 2")
        self.assertEqual(GameControllerState.GAME_NOT_STARTED, gc.state)

    def test_check_players_names_valid(self):
        gc = GameController()
        gc.add_player("Player 1")
        gc.add_player("Player 2")
        gc.check_players()
        self.assertEqual(GameControllerState.GAME_READY, gc.state)

    def test_check_players_invalid_names(self):
        gc = GameController()
        gc.add_player(123)
        gc.add_player(456)
        gc.check_players()
        self.assertEqual(GameControllerState.GAME_COMPLETE, gc.state)
        self.assertEqual(RpcResult.NO_RESULT, gc.result)

    def test_check_players_invalid_player_1_name(self):
        gc = GameController()
        gc.add_player(123)
        gc.add_player("Player 2")
        gc.check_players()
        self.assertEqual(GameControllerState.GAME_COMPLETE, gc.state)
        self.assertEqual(RpcResult.PLAYER_2_WIN, gc.result)

    def test_check_players_invalid_player_2_name(self):
        gc = GameController()
        gc.add_player("Player 1")
        gc.add_player(456)
        gc.check_players()
        self.assertEqual(GameControllerState.GAME_COMPLETE, gc.state)
        self.assertEqual(RpcResult.PLAYER_1_WIN, gc.result)

    def test_play_round_valid_player_1_win(self):
        gc = GameController()
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYERS, gc.state)
        gc.add_player("Player 1")
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYER2, gc.state)
        gc.add_player("Player 2")
        self.assertEqual(GameControllerState.GAME_NOT_STARTED, gc.state)
        gc.check_players()
        self.assertEqual(GameControllerState.GAME_READY, gc.state)
        gc.play_round("rock", "scissors")
        self.assertEqual(GameControllerState.GAME_ONGOING, gc.state)
        gc.play_round("paper", "scissors")
        self.assertEqual(GameControllerState.GAME_ONGOING, gc.state)
        gc.play_round("scissors", "paper")
        self.assertEqual(GameControllerState.GAME_COMPLETE, gc.state)
        self.assertEqual(RpcResult.PLAYER_1_WIN, gc.result)

    def test_play_round_valid_player_2_win(self):
        gc = GameController()
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYERS, gc.state)
        gc.add_player("Player 1")
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYER2, gc.state)
        gc.add_player("Player 2")
        self.assertEqual(GameControllerState.GAME_NOT_STARTED, gc.state)
        gc.check_players()
        self.assertEqual(GameControllerState.GAME_READY, gc.state)
        gc.play_round("rock", "scissors")
        self.assertEqual(GameControllerState.GAME_ONGOING, gc.state)
        gc.play_round("paper", "scissors")
        self.assertEqual(GameControllerState.GAME_ONGOING, gc.state)
        gc.play_round("paper", "scissors")
        self.assertEqual(GameControllerState.GAME_COMPLETE, gc.state)
        self.assertEqual(RpcResult.PLAYER_2_WIN, gc.result)

    def test_play_round_valid_draw(self):
        gc = GameController()
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYERS, gc.state)
        gc.add_player("Player 1")
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYER2, gc.state)
        gc.add_player("Player 2")
        self.assertEqual(GameControllerState.GAME_NOT_STARTED, gc.state)
        gc.check_players()
        self.assertEqual(GameControllerState.GAME_READY, gc.state)
        gc.play_round("rock", "scissors")
        self.assertEqual(GameControllerState.GAME_ONGOING, gc.state)
        gc.play_round("paper", "scissors")
        self.assertEqual(GameControllerState.GAME_ONGOING, gc.state)
        gc.play_round("paper", "paper")
        self.assertEqual(GameControllerState.GAME_COMPLETE, gc.state)
        self.assertEqual(RpcResult.DRAW, gc.result)

    def test_play_round_invalid_player_1_bad_input(self):
        gc = GameController()
        gc.rounds = 1
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYERS, gc.state)
        gc.add_player("Player 1")
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYER2, gc.state)
        gc.add_player("Player 2")
        self.assertEqual(GameControllerState.GAME_NOT_STARTED, gc.state)
        gc.check_players()
        self.assertEqual(GameControllerState.GAME_READY, gc.state)
        gc.play_round("xxx", "paper")
        self.assertEqual(GameControllerState.GAME_COMPLETE, gc.state)
        self.assertEqual(RpcResult.PLAYER_2_WIN, gc.result)

    def test_play_round_invalid_player_2_bad_input(self):
        gc = GameController()
        gc.rounds = 1
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYERS, gc.state)
        gc.add_player("Player 1")
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYER2, gc.state)
        gc.add_player("Player 2")
        self.assertEqual(GameControllerState.GAME_NOT_STARTED, gc.state)
        gc.check_players()
        self.assertEqual(GameControllerState.GAME_READY, gc.state)
        gc.play_round("paper", "xxx")
        self.assertEqual(GameControllerState.GAME_COMPLETE, gc.state)
        self.assertEqual(RpcResult.PLAYER_1_WIN, gc.result)

    def test_play_round_1_round_player_1_win(self):
        gc = GameController()
        gc.rounds = 1
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYERS, gc.state)
        gc.add_player("Player 1")
        self.assertEqual(GameControllerState.WAITING_FOR_PLAYER2, gc.state)
        gc.add_player("Player 2")
        self.assertEqual(GameControllerState.GAME_NOT_STARTED, gc.state)
        gc.check_players()
        self.assertEqual(GameControllerState.GAME_READY, gc.state)
        gc.play_round("rock", "scissors")
        self.assertEqual(GameControllerState.GAME_COMPLETE, gc.state)
        self.assertEqual(RpcResult.PLAYER_1_WIN, gc.result)

if __name__ == '__main__':
    unittest.main()